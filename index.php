<?php

require('animal.php');

$sheep = new Animal("shaun");
echo "Name : " .$sheep->name. "<br>"; // "shaun"
echo "Legs : " .$sheep->legs. "<br>"; // 4
echo "Cold Blooded : " .$sheep->cold_blooded. "<br>"; // "no"
echo "<br><br>";
require('Ape.php');

$Kera_Sakti = new Animal1("Kera Sakti");
echo "Name : " .$Kera_Sakti->name. "<br>"; 
echo "Legs : " .$Kera_Sakti->legs. "<br>"; 
echo "Cold Blooded : " .$Kera_Sakti->cold_blooded. "<br>"; 
echo "Yell : " .$Kera_Sakti->yell. "<br>";

echo "<br><br>";
require('Frog.php');

$Buduk = new Animal2("Buduk");
echo "Name : " .$Buduk->name. "<br>"; 
echo "Legs : " .$Buduk->legs. "<br>"; 
echo "Cold Blooded : " .$Buduk->cold_blooded. "<br>"; 
echo "Jump : " .$Buduk->jump. "<br>";

?>